﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace ZHJ.PerfectOA.DALFactory
{
    public class DBSessionFactory
    {
        /// <summary>
        /// 得到DbSession对象
        /// </summary>
        /// <returns></returns>
        public static IDAL.IDBSession GetCurrentDbSession()
        {
            //将DbSession对象重CallContext取出
            IDAL.IDBSession DbSession = (IDAL.IDBSession)CallContext.GetData("dbSession");
            if (DbSession == null)  //如果没有  创建，将DbSession存到CallContext中
            {
                DbSession = new DBSession();
                CallContext.SetData("dbSession", DbSession);
            }
            return DbSession;
        }
    }
}
