﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using ZHJ.PerfectOA.Model;

namespace ZHJ.PerfectOA.DAL
{
    /// <summary>
    /// 负责EF上下文的创建，保证线程内唯一.
    /// </summary>
    public class DbContextFactory
    {
        /// <summary>
        /// 创建EF上下文对象，首先判断DbContext中有没有dbContext 如果没有直接创建
        /// </summary>
        /// <returns></returns>
        public static DbContext CreateCurrentDbContext()
        {
            DbContext dbContext = (DbContext)CallContext.GetData("dbContext");
            if (dbContext ==null)
            {
                //将EF对象赋值到dbContext
                dbContext = new OAEntities();
                CallContext.SetData("dbContext", dbContext);
            }
            return dbContext;
        }
    }
}
