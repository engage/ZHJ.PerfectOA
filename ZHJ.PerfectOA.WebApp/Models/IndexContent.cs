﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZHJ.PerfectOA.Model.Enum;

namespace ZHJ.PerfectOA.WebApp.Models
{
    public class IndexContent
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Msg { get; set; }
        public LuceneEnumType LuceneEnumType { get; set; }
    }
}