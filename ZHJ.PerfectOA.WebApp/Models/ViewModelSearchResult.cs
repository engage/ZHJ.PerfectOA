﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZHJ.PerfectOA.WebApp.Models
{
    public class ViewModelSearchResult
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Msg { get; set; }
        public string Url { get; set; }
    }
}