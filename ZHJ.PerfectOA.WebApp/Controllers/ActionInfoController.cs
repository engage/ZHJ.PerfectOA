﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZHJ.PerfectOA.Model;
using ZHJ.PerfectOA.Model.Enum;

namespace ZHJ.PerfectOA.WebApp.Controllers
{
    public class ActionInfoController : Controller
    {
        //
        // GET: /ActionInfo/
        IBLL.IActionInfoService ActionInfoService { get; set; }
        IBLL.IRoleInfoService RoleInfoService { get; set; }

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 分页展示数据
        /// </summary>
        /// <returns></returns>
        public ActionResult GetActionInfo()
        {
            int pageIndex = Request["page"] != null ? int.Parse(Request["page"]) : 1;
            int pageSize = Request["rows"] != null ? int.Parse(Request["rows"]) : 5;
            int totalCount;
            short delFlag = (short)DelFlags.Normal;
            var actionInfoList = ActionInfoService.LoadPageEntities<int>(pageIndex, pageSize, out totalCount, c => c.DelFlag == delFlag, c => c.ID, true);
            var temp = from r in actionInfoList
                       select new { ID = r.ID, ActionInfoName = r.ActionInfoName, Sort = r.Sort, Remark = r.Remark, SumTime = r.SubTime, Url = r.Url, HttpMethod = r.HttpMethod, ActionTypeEnum = r.ActionTypeEnum };
            return Json(new { rows = temp, total = totalCount }, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <returns></returns>
        public ActionResult FileUpload()
        {
            HttpPostedFileBase file = Request.Files["fileImage"];
            if (file != null)
            {
                string fileName = Path.GetFileName(file.FileName);
                string fileExt = Path.GetExtension(fileName);
                if (fileExt == ".jpg")
                {
                    string dir = "/ImageFileUp/" + DateTime.Now.Year + "/" + DateTime.Now.Month + "/" + DateTime.Now.Day + "/";
                    Directory.CreateDirectory(Path.GetDirectoryName(Request.MapPath(dir)));
                    string newfileName = Guid.NewGuid().ToString();
                    string fullDir = dir + newfileName + fileExt;
                    file.SaveAs(Request.MapPath(fullDir));
                    return Content("ok:" + fullDir);
                }
                else
                {
                    return Content("no:文件类型错误!!");
                }
            }
            else
            {
                return Content("no:文件不能为空");
            }
        }

        /// <summary>
        /// 删除操作
        /// </summary>
        /// <returns></returns>
        public ActionResult DeleteActionInfo()
        {
            //分割id字符串 将每个id加到list集合中
            string strId = Request["strId"];
            string[] ids = strId.Split(',');
            List<int> list = new List<int>();
            foreach (var id in ids)
            {
                list.Add(int.Parse(id));
            }

            if (ActionInfoService.DeleteEntities(list))
            {
                return Content("ok");
            }
            else
            {
                return Content("no");
            }
        }

        /// <summary>
        /// 添加权限方法
        /// </summary>
        /// <param name="actionInfo"></param>
        /// <returns></returns>
        public ActionResult AddActionInfo(ActionInfo actionInfo)
        {
            actionInfo.DelFlag = 0;
            actionInfo.ModifiedOn = DateTime.Now.ToString();
            actionInfo.SubTime = DateTime.Now;
            actionInfo.Url = actionInfo.Url.ToLower();
            ActionInfoService.AddEntity(actionInfo);
            return Content("ok");
        }

        /// <summary>
        /// 修改要显示的角色信息
        /// </summary>
        /// <returns></returns>
        public ActionResult ShowEditInfo()
        {
            int id = int.Parse(Request["id"]);
            ActionInfo actionInfo = ActionInfoService.LoadEntities(a => a.ID == id).FirstOrDefault();
            ViewData.Model = actionInfo;
            return View();
        }

        /// <summary>
        /// 保存修改之后的信息
        /// </summary>
        /// <returns></returns>
       [HttpPost]
        public ActionResult ShowEditInfo(ActionInfo actionInfo)
        {
            actionInfo.DelFlag = 0;

            if (ActionInfoService.EditEntity(actionInfo))
            {
                return Content("ok");
            }
            else
            {
                return Content("no");
            }
        }

        /// <summary>
        /// 给权限分配角色
        /// </summary>
        /// <returns></returns>
       public ActionResult SetActionRoleInfo()
       {
           int id = int.Parse(Request["id"]);//权限编号.
           var actionInfo = ActionInfoService.LoadEntities(a => a.ID == id).FirstOrDefault();
           ViewBag.ActionInfo = actionInfo;
           short delFlag = (short)DelFlags.Normal;
           ViewBag.AllRoleList = RoleInfoService.LoadEntities(r => r.DelFlag == delFlag).ToList();//获取所有的角色.
           ViewBag.AllRoleIdList = (from r in actionInfo.RoleInfo
                                    select r.ID).ToList();
           return View();

       }

       [HttpPost]
       public ActionResult SetActionRoleInfo(ActionInfo actionInfo)
       {
           int actionId = int.Parse(Request["actionId"]);
           string[] AllKeys = Request.Form.AllKeys;
           List<int> list = new List<int>();
           foreach (string key in AllKeys)
           {
               if (key.StartsWith("cba_"))
               {
                   string k = key.Replace("cba_", "");
                   list.Add(int.Parse(k));
               }
           }
           if (ActionInfoService.SetActionInfoRole(actionId, list))
           {
               return Content("ok");
           }
           else
           {
               return Content("no");
           }
       }
    }
}
