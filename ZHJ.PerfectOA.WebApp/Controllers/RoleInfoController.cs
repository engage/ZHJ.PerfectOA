﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZHJ.PerfectOA.Model;
using ZHJ.PerfectOA.Model.Enum;

namespace ZHJ.PerfectOA.WebApp.Controllers
{
    public class RoleInfoController : Controller
    {
        //
        // GET: /RoleInfo/
        IBLL.IRoleInfoService RoleInfoService { get; set; }
        public ActionResult Index()
        {

            return View();
        }

        #region 分页展示列表
        /// <summary>
        /// 分页展示列表
        /// </summary>
        /// <returns></returns>
        public ActionResult GetRoleInfo()
        {
            int pageIndex = Request["page"] != null ? int.Parse(Request["page"]) : 1;
            int pageSize = Request["rows"] != null ? int.Parse(Request["rows"]) : 5;
            int totalCount;
            short delflage = (short)DelFlags.Normal;
            var rowInfoList = RoleInfoService.LoadPageEntities<int>(pageIndex, pageSize, out totalCount, c => c.DelFlag == delflage, c => c.ID, true);
            var temp = from r in rowInfoList
                       select new { ID = r.ID, RoleName = r.RoleName, Sort = r.Sort, Remark = r.Remark, SubTime = r.SubTime };
            //将数据JSON化之后返回客户端，并且制定是来自客户端的Get请求方法的
            return Json(new { rows = temp, total = totalCount }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region 添加角色
        /// <summary>
        /// 添加角色
        /// </summary>
        /// <param name="roleInfo"></param>
        /// <returns></returns>
        public ActionResult AddRoleInfo(RoleInfo roleInfo)
        {
            roleInfo.DelFlag = 0;
            roleInfo.ModifiedOn = DateTime.Now;
            roleInfo.SubTime = DateTime.Now;
            RoleInfoService.AddEntity(roleInfo);
            return Content("ok");
        }
        #endregion

        #region 批量删除方法
        /// <summary>
        /// 批量删除方法
        /// </summary>
        /// <returns></returns>
        public ActionResult DeleteRoleInfo()
        {
            //分割id字符串 将每个id加到list集合中
            string strId = Request["strId"];
            string[] ids = strId.Split(',');
            List<int> list = new List<int>();
            foreach (var id in ids)
            {
                list.Add(int.Parse(id));
            }

            if (RoleInfoService.DeleteEntities(list))
            {
                return Content("ok");
            }
            else
            {
                return Content("no");
            }
        }
        #endregion

        /// <summary>
        /// 渲染修改模板
        /// </summary>
        /// <returns></returns>
        public ActionResult EditInfo()
        {
            int id = int.Parse(Request["id"]);
            RoleInfo roleInfo = RoleInfoService.LoadEntities(r => r.ID == id).FirstOrDefault();
            ViewData.Model = roleInfo;
            return View();
        }

        #region 修改角色信息
        /// <summary>
        /// 修改角色信息
        /// </summary>
        /// <param name="roleInfo"></param>
        /// <returns></returns>
        public ActionResult EditRoleInfo(RoleInfo roleInfo)
        {
            if ( RoleInfoService.EditEntity(roleInfo))
            {
                return Content("ok");
            }
            else
            {
                return Content("no");
            }
           
        }
        #endregion
    }
}
