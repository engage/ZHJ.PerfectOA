﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZHJ.PerfectOA.IBLL;
using ZHJ.PerfectOA.Model;

namespace ZHJ.PerfectOA.BLL
{
    public partial class KeyWordsRankService : BaseService<KeyWordsRank>, IKeyWordsRankService
    {
        /// <summary>
        /// 删除KeyWordsRank表
        /// </summary>
        public void DeleteKeyWord()
        {
            string sql = "truncate table KeyWordsRank";
            this.GetDbSession.ExecuteSql(sql);

        }
        
        /// <summary>
        /// 将数据写到KeyWordsRank表中
        /// </summary>
        public void InsertKeyWord()
        {
            string sql = "insert into KeyWordsRank(Id,KeyWords,SearchCount) select newId(),KeyWords, count(*) from SearchDetails where DateDiff(day,SearchDateTime,getdate())<=7 group by KeyWords";
            this.GetDbSession.ExecuteSql(sql);
        }
        /// <summary>
        /// 查找与查找内容相仿的词
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public List<string> GetTerm(string msg)
        {
            string sql = "select  KeyWords from KeyWordsRank where KeyWords like @msg ";
            return this.GetDbSession.ExecuteQuerySql<string>(sql, new System.Data.SqlClient.SqlParameter("@msg", msg + "%"));
        }
    }
}
