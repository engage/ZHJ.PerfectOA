﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZHJ.PerfectOA.IBLL;
using ZHJ.PerfectOA.Model;

namespace ZHJ.PerfectOA.BLL
{
    public partial class ActionInfoService :IActionInfoService
    {

        /// <summary>
        /// 给权限分配角色
        /// </summary>
        /// <param name="actionId"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public bool SetActionInfoRole(int actionId, List<int> list)
        { 
            var actionInfo = this.GetDbSession.ActionInfoDal.LoadEntities(a => a.ID == actionId).FirstOrDefault();
            if (actionInfo != null)
            {
                actionInfo.RoleInfo.Clear();
                foreach (int roleId in list)
                {
                    var roleInfo = this.GetDbSession.RoleInfoDal.LoadEntities(r => r.ID == roleId).FirstOrDefault();
                    actionInfo.RoleInfo.Add(roleInfo);
                }
            }
            return this.GetDbSession.SaveChanges();
        }

        /// <summary>
        /// 批量删除记录
        /// </summary>
        /// <param name="list">要删除的记录编号(ID)</param>
        /// <returns></returns>
        public bool DeleteEntities(List<int> list)
        {
            var actionInfoList = this.GetDbSession.ActionInfoDal.LoadEntities(u => list.Contains(u.ID));
            foreach (var actionInfo in actionInfoList)
            {
                this.GetDbSession.ActionInfoDal.DeleteEntity(actionInfo);
            }
            return this.GetDbSession.SaveChanges();
        } 
    }
}
