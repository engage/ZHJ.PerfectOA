﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZHJ.PerfectOA.Model.Enum
{
    /// <summary>
    /// 数据删除标识
    /// </summary>
    public enum DelFlags
    {
        /// <summary>
        /// 正常
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 逻辑删除
        /// </summary>
        LogicDelete = 1,
        /// <summary>
        /// 物理删除
        /// </summary>
        PhyicDelete = 2
    }
}
