﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZHJ.PerfectOA.Model;

namespace ZHJ.PerfectOA.IBLL
{
    public partial interface IActionInfoService
    {
        bool DeleteEntities(List<int> list);
        bool SetActionInfoRole(int actionId, List<int> list);
    }
}
