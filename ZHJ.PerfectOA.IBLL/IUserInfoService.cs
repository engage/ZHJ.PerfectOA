﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZHJ.PerfectOA.Model;

namespace ZHJ.PerfectOA.IBLL
{
    public partial interface IUserInfoService : IBaseService<UserInfo>
    {
        bool DeleteEntities(List<int> list);

        bool SetUserInfoRole(int userId, List<int> list);
    }
}
